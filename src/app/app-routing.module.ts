import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageIdentityComponent } from './manage-identity/manage-identity.component';
import { ConfigurationComponent } from './configuration/configuration.component';

const routes: Routes = [
  {path : 'manageIdentity', component :  ManageIdentityComponent},
  {path :'configuration',component  : ConfigurationComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
