import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatIconModule } from "@angular/material/icon";
import { HttpClientModule } from "@angular/common/http";
import { MatAutocompleteModule,
  
  MatButtonModule,
 
  MatCardModule,
 
  MatMenuModule,

  MatListModule,

} from '@angular/material';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ManageIdentityComponent } from './manage-identity/manage-identity.component';
import { ConfigurationComponent } from './configuration/configuration.component';

@NgModule({
  declarations: [
    AppComponent,
    
    ManageIdentityComponent,
    
    ConfigurationComponent
  ],
  imports: [
    BrowserModule,

    AppRoutingModule,

    MatAutocompleteModule,

    MatButtonModule,
 
    MatCardModule,
   
    MatIconModule,
   
    MatMenuModule,
  
    MatListModule,

    HttpClientModule,

    

    NgbModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
